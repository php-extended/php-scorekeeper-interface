<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-scorekeeper-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Scorekeeper;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * ScorekeeperInterface interface file.
 * 
 * This interface defines the basic features of a scorekeeper.
 * 
 * @author Anastaszor
 */
interface ScorekeeperInterface extends Stringable
{
	
	/**
	 * Gets the score for the given namespace, class name and field name. If
	 * there is no score for the given namespace, class name and field name,
	 * a default neutral score is returned.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $fieldname
	 * @return ScoreInterface the score to retrieve
	 */
	public function getScore(string $namespace, string $classname, string $fieldname) : ScoreInterface;
	
	/**
	 * Sets the given score according to its namespace, class name and field name.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $fieldname
	 * @param ScoreInterface $score the score value to set
	 * @return boolean whether the score was successfully set
	 */
	public function setScore(string $namespace, string $classname, string $fieldname, ScoreInterface $score) : bool;
	
}
